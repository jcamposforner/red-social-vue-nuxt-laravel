<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->float('price', 8,2);
            $table->float('rating',8,2);
            $table->integer('viewed');
            $table->dateTime('lastBuy');
            $table->integer('quantity');
            $table->integer('quantityBuy');
            $table->string('brand');
            $table->boolean('active')->default(0);
            $table->string('image');

            $table->integer('subfamily_id');
            $table->string('slug');


            $table->string('metatitle');
            $table->text('metadescription');
            $table->string('ogImage');
            $table->text('ogDescription');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
