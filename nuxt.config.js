const laravelNuxt = require("laravel-nuxt");

module.exports = laravelNuxt({
  // Options such as mode, srcDir and generate.dir are already handled for you.
  head: {
    title: 'Red Social',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '{{ description }}' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Nunito:300,400,500,700|Material+Icons' }
    ]
  },
  modules: [],
  plugins: [
    '~plugins/vuetify.js',
    '~plugins/mtoast.js',
    '~plugins/chartjs.js'
  ],
  css: [
    '~assets/css/app.styl',
  ],
  build: {
      vendor: ['vuetify'],
  }
});