<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Services\UploadController;
use Illuminate\Http\Request;

class FriendsController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt');

    }

    public function upload(Request $request, UploadController $uploadController)
    {

        $file = $uploadController->uploadImage($request->file('avatar'),'avatars');

        return response([
            'data' => $file,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payload = $request->attributes->get('payload');
        $me = User::find($payload->id);

        $friends = $me->friends()->paginate(15);

        return response([
            'status' => 'success',
            'data' => $friends,
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payload = $request->attributes->get('payload');

        if($payload->id == $request->id)
        {
            return response([
                'status' => 'error',
                'msg' => 'No puedes agregarte a ti mismo'
            ],500);
        }


        $me = User::find($payload->id);
        $friend = User::find($request->id);

        $me->addfriend($friend);

        return response([
            'status' => 'success',
            'data'  => $friend,
        ],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $payload = $request->attributes->get('payload');



        $me = User::find($payload->id);
        $friend = User::find($id);

        $me->removeFriend($friend);

        return response([
            'status' => 'success',
            'data'  => $friend,
        ],200);
    }
}
