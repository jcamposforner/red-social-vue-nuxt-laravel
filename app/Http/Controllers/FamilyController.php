<?php

namespace App\Http\Controllers;

use App\Http\Services\UploadController;
use Illuminate\Http\Request;
use App\Family;
use App\Http\Requests\FamilyFormRequest;


class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $families = Family::all();

        return response([
            'status' => 'success',
            'data' => $families,
        ],200);
    }

    public function uploadImage(Request $request, UploadController $uploadController, $slug)
    {
        $family = Family::where('slug',$slug)->first();

        $file = $uploadController->uploadImage($request->file('image'),'family');

        $family->image = $file['data'];

        $family->save();

        return response([
            'data' => $file,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FamilyFormRequest $request)
    {
        $family = new Family;

        $family->name = $request->name;
        $family->description = $request->description;
        $family->active = $request->active;
        $family->image = $request->image;
        $family->metatitle = $request->metatitle;
        $family->metadescription = $request->metadescription;
        $family->ogImage = $request->ogImage;
        $family->ogDescription = $request->ogDescription;

        $family->slug = str_slug($request->name, "-");

        $family->save();


        return response([
            'status' => 'success',
            'data' => $family
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $family = Family::where('slug',$slug)->first();
        $family->subfamily;

        return response([
            'status' => 'success',
            'data' => [
                'family' => $family,
                ],
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $family = Family::find($id);
        $family->delete();

        return response([
            'status' => 'success',
            'data' => $family,
        ],200);
    }
}
