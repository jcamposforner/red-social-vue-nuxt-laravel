<?php

namespace App\Http\Controllers;

use App\Family;
use App\Http\Requests\SubFamilyFormRequest;
use App\Http\Services\UploadController;
use App\Subfamily;
use Illuminate\Http\Request;

class SubfamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subfamily = Subfamily::all();

        return response([
            'status' => 'success',
            'data' => $subfamily,
        ],200);
    }

    public function uploadImage(Request $request, UploadController $uploadController)
    {
        $subfamily = Family::where('slug',$slug)->first();

        $file = $uploadController->uploadImage($request->file('image'),'subfamily');

        $subfamily->image = $file['data'];

        $subfamily->save();

        return response([
            'data' => $file,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubFamilyFormRequest $request)
    {
        $subfamily = new Subfamily();

        $subfamily->name = $request->name;
        $subfamily->description = $request->description;
        $subfamily->active = $request->active;
        $subfamily->image = $request->image;
        $subfamily->metatitle = $request->metatitle;
        $subfamily->metadescription = $request->metadescription;
        $subfamily->ogImage = $request->ogImage;
        $subfamily->ogDescription = $request->ogDescription;

        $subfamily->family_id = $request->family_id;

        $subfamily->slug = str_slug($request->name, "-");

        $subfamily->save();


        return response([
            'status' => 'success',
            'data' => $subfamily
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $subfamily = Subfamily::where('slug',$slug)->first();
        $subfamily->family;
        $products = $subfamily->products()->paginate(15);

        return response([
            'status' => 'success',
            'data' => [
                'subfamily' => $subfamily,
                'products' => $products
            ],
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subfamily = Subfamily::find($id);
        $subfamily->delete();

        return response([
            'status' => 'success',
            'data' => $subfamily,
        ],200);
    }
}
