<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductFormRequest;
use App\Http\Services\UploadController;
use App\Products;
use App\Subfamily;
use Illuminate\Http\Request;


class ProductsController extends Controller
{


    public function __construct()
    {

        $this->middleware('isAdmin',[
            'except' => [
                'show'
            ]
        ]);
    }



    public function uploadImage(Request $request, UploadController $uploadController)
    {
        $product = Products::where('slug',$slug)->first();

        $file = $uploadController->uploadImage($request->file('image'),'products');

        $product->image = $file['data'];

        $product->save();

        return response([
            'data' => $file,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductFormRequest $request)
    {
        $product = new Products;

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->rating = $request->rating;
        $product->viewed = $request->viewed;
        $product->lastBuy = $request->lastBuy;
        $product->quantity = $request->quantity;
        $product->quantityBuy = $request->quantityBuy;
        $product->brand = $request->brand;
        $product->active = $request->active;
        $product->image = $request->image;
        $product->subfamily_id = $request->subfamily_id;
        $product->metatitle = $request->metatitle;
        $product->metadescription = $request->metadescription;
        $product->ogImage = $request->ogImage;
        $product->ogDescription = $request->ogDescription;

        $product->slug = str_slug($request->name, "-");

        $product->save();

        return response([
            'status' => 'success',
            'data'  => $product,
        ],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Products::where('slug',$slug)->first();
        return response([
            'status' => 'success',
            'data' => $product
        ],200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Products::find($id);

        $product->name = $request->name ? $request->name : $product->name;
        $product->description = $request->description ? $request->description : $product->description;
        $product->price = $request->price ? $request->price : $product->price;
        $product->rating = $request->rating ? $request->rating : $product->rating;
        $product->viewed = $request->viewed ? $request->viewed : $product->viewed;
        $product->lastBuy = $request->lastBuy ? $request->lastBuy : $product->lastBuy;
        $product->quantity = $request->quantity ? $request->quantity : $product->quantity;
        $product->quantityBuy = $request->quantityBuy ? $request->quantityBuy : $product->quantityBuy;
        $product->brand = $request->brand ? $request->brand : $product->brand;
        $product->active = $request->active ? $request->active : $product->active;
        $product->image = $request->image ? $request->image : $product->image;
        $product->subfamily_id = $request->subfamily_id ? $request->subfamily_id : $product->subfamily_id;
        $product->metatitle = $request->metatitle ? $request->metatitle : $product->metatitle;
        $product->metadescription = $request->metadescription ? $request->metadescription : $product->metadescription;
        $product->ogImage = $request->ogImage ? $request->ogImage : $product->ogImage;
        $product->ogDescription = $request->ogDescription ? $request->ogDescription : $product->ogDescription;

        $product->save();

        return response([
            'status' => 'success',
            'data'  => $product
        ],200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Products::find($id);
        $product->delete();

        return response([
            'status' => 'success',
            'data' => $product,
        ],200);
    }
}
