<?php

namespace App\Http\Controllers;

use App\Http\Services\UploadController;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterFormRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use JWTFactory;

class AuthController extends Controller
{


    public function register(RegisterFormRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->nick = $request->nick;
        $user->password = bcrypt($request->password);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    public function uploadImage(Request $request, UploadController $uploadController)
    {

        $payload = $request->attributes->get('payload');

        $user = User::find($payload->id);

        $file = $uploadController->uploadImage($request->file('image'),'users');

        $user->image = $file['data'];

        $user->save();

        return response([
            'data' => $file,
        ]);
    }

    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        $user = User::where([
            'email'=>$credentials['email'],
        ])->first();

        if(!$user){
            return response([
                'status' => 'fail',
                'message' => 'No existe el usuario'
            ],404);
        }

        if(Hash::check($credentials['password'],$user['password'])) {
            $token = JWTAuth::fromUser($user);
            
            if ($token) {
                return response()->json([
                    'token' => 'Bearer '.$token,
                    'user' => $user,
                ]);
            } else {
                return response()->json([
                    'code' => 2,
                    'message' => 'Credenciales no válidos.'
                ]);
            }
        }else {
            return response()->json([
                'code' => 2,
                'message' => 'Credenciales no válidos.'
            ]);
        }
    }
}
