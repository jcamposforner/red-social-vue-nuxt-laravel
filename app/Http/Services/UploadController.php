<?php

namespace App\Http\Services;

class UploadController
{

    public function UploadImage($file, $location)
    {

        $validExtensions = [
            'jpg',
            'png'
        ];
        $validMimeType = [
            'image/jpeg',
            'image/png'
        ];

        if(in_array($file->getClientOriginalExtension(), $validExtensions) && in_array($file->getClientMimeType(), $validMimeType))
        {
            $path = $file->store($location);

            return [
                'status' => 'success',
                'data' => $path,
            ];
        }
        else
        {
            return [
                'status' => 'error',
                'data' => 'No se puede subir ese tipo de imagen'
            ];
        }

    }
}
