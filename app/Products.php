<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{


    public function subfamily()
    {
        return $this->belongsTo('App\Subfamily');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

}
