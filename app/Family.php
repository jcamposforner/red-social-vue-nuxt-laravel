<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{


    public function subfamily()
    {
        return $this->hasMany('App\Subfamily');
    }
}
