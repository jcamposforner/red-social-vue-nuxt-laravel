<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subfamily extends Model
{

    public function products()
    {
        return $this->hasMany('App\Products');
    }

    public function family()
    {
        return $this->belongsTo('App\Family');
    }
}
