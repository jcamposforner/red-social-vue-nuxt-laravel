<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');

Route::post('test', 'FriendsController@upload');



Route::resource('family', 'FamilyController');
Route::resource('subfamily', 'SubfamilyController');
Route::resource('product', 'ProductsController');
Route::resource('comment', 'CommentsController');
Route::resource('friends', 'FriendsController');


// Subida de ficheros
Route::post('upload-image-user', 'AuthController@uploadImage')->middleware('jwt');
Route::post('upload-image-family', 'FamilyController@uploadImage')->middleware('isAdmin');
Route::post('upload-image-subfamily', 'SubfamilyController@uploadImage')->middleware('isAdmin');
Route::post('upload-image-product', 'ProductsController@uploadImage')->middleware('isAdmin');


Route::get('me', function () {
    // Let's return fake information.
    return [
        'name' => 'John Doe',
    ];
});