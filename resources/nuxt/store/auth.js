export const state = () => ({
    authSuccess: '',
    identity: {},
    token: {},
    dialog: false,
});

export const mutations = {
    successLoginMutations( state,payload ){
        console.log(payload)
    },
    errorLoginMutations( state ){
        state.dialog = true;
    },
    modal( state ){
        state.dialog = false;
    }
};

export const actions = {
    async login({commit}, payload) {
        await this.$axios.post('/api/auth/login', payload).then((res) => {
            if(res.data.token){
                localStorage.setItem('identity', JSON.stringify(res.data));
                commit('successLoginMutations', res.data);
            }else{
                commit('errorLoginMutations', res.data);
            }
        });

    },
    async register({commit}, payload) {
        await this.$axios.post('/api/auth/register', payload).then((res) => {
            commit('successLoginMutations', res.data);
        }).catch((err) => {
            commit('errorLoginMutations');
        });

    }
};