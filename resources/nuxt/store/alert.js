export const state = () => ({
    type: null,
    message: null,
});

export const mutations = {
    success(state, message) {
        state.type= 'alert-success';
        state.message = message;
    },
    error(state, message) {
        state.type= 'alert-error';
        state.message = message;
    }
};